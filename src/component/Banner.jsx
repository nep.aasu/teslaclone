import React from "react";

const Banner = () => {
  return (
    <div className=" flex flex-col md:flex-row justify-between gap-x-6  px-6 py-4 ">
      <div className="flex flex-col md:flex-row justify-between gap-x-10 ">
        <h5 className="text-xs md:text-sm ">$7,500 Federal Tax Credit</h5>
        <p className="text-xs md:text-sm text-start md:text-center">
          {" "}
          Eligible buyers now receive $7,500 off the purchase price of new 2024
          Model Y and Model X Dual Motor. Applied at time of delivery.
        </p>
      </div>
      <div>
        <p className="text-xs md:text-sm ">See Details</p>
      </div>
    </div>
  );
};

export default Banner;
