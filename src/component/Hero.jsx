import { hero } from "../constant";
import Button from "./Button";
const Hero = () => {
  return (
    <section className="flex flex-col overflow-y-scroll h-screen  snap-y snap-mandatory">
      {hero.map(({ id, img, title, desc, button, video, desc1 }, index) => (
        <div
          key={id}
          className={`relative flex flex-col items-center justify-center h-screen snap-always  snap-center ${
            id === "modelY" || id === "experienceTesla" ? "text-white" : ""
          } ${
            index === hero.length - 1
              ? "scroll-snap-align-end"
              : "scroll-snap-align-start"
          }`}
        >
          {img && (
            <img
              src={img}
              alt={title}
              className="h-screen w-full object-cover"
            />
          )}
          {video && (
            <video className="h-screen w-full object-cover" autoPlay muted loop>
              <source src={video} type="video/mp4" />
              Your browser does not support the video tag.
            </video>
          )}
          <div className="absolute top-20 flex flex-col h-screen w-full items-center justify-between">
            <div className="flex flex-col items-center">
              <h1 className="text-4xl font-medium sm:text-2xl">{title}</h1>
              <p className="text-sm sm:text-xl sm:text-base ">{desc}</p>
            </div>
            <div className="flex flex-col w-full gap-y-6 mb-24 sm:flex">
              <Button button={button} />
              <p className="mb-24 text-xs text-center  sm:text-sm">{desc1}</p>
            </div>
          </div>
        </div>
      ))}
    </section>
  );
};

export default Hero;
