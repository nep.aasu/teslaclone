import React, { useState } from "react";
import { logo } from "../assets";
import { navLink, rightNavLink } from "../constant";

const Navbar = () => {
  const [showNav, setShowNav] = useState(false);
  const [selectedNavLink, setSelectedNavLink] = useState(null);
  const [showMenu, setShowMenu] = useState(false);
  const [showMegaMenu, setShowMegaMenu] = useState(false);

  const handleMenuHover = (id) => {
    setSelectedNavLink(id);
    setShowNav(true);
  };

  const handleDropdownLeave = () => {
    setShowNav(false);
  };
  const handleMenu = () => {
    setShowMenu(!showMenu);
  };

  const handleMegaMenu = (id) => {
    console.log("click");
    setSelectedNavLink(id);
    setShowMegaMenu(!showMegaMenu);
  };
  return (
    <header
      className={`fixed top-15 left-0 right-0 z-10 flex justify-between px-12 py-4 transition-all duration-500 ${
        showNav ? "bg-white" : ""
      }`}
    >
      <img src={logo} alt="logo" className="w-28 object-contain" />
      <div className="hidden item-center lg:flex gap-x-2 ">
        {navLink.map(
          ({
            title,
            id,
            dropDownContent,
            dropDownContentRight,
            dropDownCenter,
            resource,
            locationServices,
            about,
          }) => (
            <div key={id}>
              <p
                className="cursor-pointer text-sm rounded-md py-[6px] px-2 font-medium hover:bg-gray-200 transition-all duration-500 "
                onMouseEnter={() => handleMenuHover(id)}
              >
                {title}
              </p>
              {showNav && selectedNavLink === id && (
                <div
                  className="absolute top-14 right-0 left-0 bg-white opacity-100 transition-opacity duration-300 transform translate-y-0 hover:translate-y-1"
                  onMouseEnter={() => handleMenuHover(id)}
                  onMouseLeave={handleDropdownLeave}
                >
                  <div className=" py-12">
                    {dropDownContent ? (
                      <div className="grid grid-cols-8 gap-4">
                        <div className="flex flex-wrap col-start-2 col-span-4  border-r-2 ">
                          {dropDownContent.map(({ title, id, link, img }) => (
                            <div
                              key={id}
                              className="p-2 text-center items-center"
                            >
                              <img src={img} alt="" className="w-40" />
                              <p className="text-md font-semibold">{title}</p>
                              {link ? (
                                <div className="flex  justify-center gap-2">
                                  {link.map((value, i) => (
                                    <p className="text-xs font-light">
                                      {value}
                                    </p>
                                  ))}
                                </div>
                              ) : null}
                            </div>
                          ))}
                        </div>
                        <div className="px-10 py-5 col-span-2">
                          {dropDownContentRight.map(({ title, id }) => (
                            <p key={id} className="text-sm py-2">
                              {title}
                            </p>
                          ))}
                        </div>
                      </div>
                    ) : (
                      <div className="flex justify-center gap-28">
                        {dropDownCenter ? (
                          <div className="flex gap-4">
                            {dropDownCenter.map(({ id, title, img }) => (
                              <div key={id}>
                                <img src={img} alt="" className="w-52  " />
                                <p className="text-center">{title}</p>
                              </div>
                            ))}
                          </div>
                        ) : (
                          <>
                            <div>
                              <p className="mb-2 text-sm text-slate-500">
                                Resources
                              </p>
                              {resource.map((item, i) => (
                                <p key={i} className="py-2 text-sm font-medium">
                                  {item}
                                </p>
                              ))}
                            </div>
                            <div>
                              <p className="mb-2 text-sm text-slate-500">
                                Location Services
                              </p>
                              {locationServices.map((item, i) => (
                                <p className="py-2 text-sm font-medium">
                                  {item}
                                </p>
                              ))}
                            </div>
                            <div>
                              <p className="mb-2 text-sm text-slate-500">
                                Company
                              </p>
                              {about.map((item, i) => (
                                <p className="py-2 text-sm font-medium">
                                  {item}
                                </p>
                              ))}
                            </div>
                          </>
                        )}
                      </div>
                    )}
                  </div>
                </div>
              )}
            </div>
          )
        )}
      </div>

      <div>
        <div>
          <div className="hidden item-center lg:flex py-1 nr-1 gap-x-1">
            {rightNavLink.map((value, i) => {
              // return <p key={i}>{value.title}</p>;
              return (
                <img
                  key={i}
                  src={value.img}
                  alt={value.title}
                  className="w-8 object-contain cursor-pointer px-1  rounded-md  hover:bg-transparent-500 "
                ></img>
              );
            })}
          </div>
        </div>
        <p
          className="lg:hidden cursor-pointer rounded-md py-[6px] px-2 font-medium hover:bg-gray-200"
          onClick={handleMenu}
        >
          Menu
        </p>
        {showMenu && (
          <div className="absolute top-0 right-0 left-0  bg-white p-4 overflow-auto h-screen">
            <div className="flex justify-end pb-10">
              <button
                onClick={handleMenu}
                className="text-xl rounded-md py-4 px-2 font-medium hover:bg-gray-200"
              >
                X
              </button>
            </div>
            {navLink.map(
              ({
                title,
                id,
                dropDownContent,
                dropDownContentRight,
                dropDownCenter,
                resource,
                locationServices,
                about,
              }) => (
                <>
                  <div
                    key={id}
                    className="cursor-pointer text-lg flex py-4 justify-between  rounded-md px-2 font-medium hover:bg-gray-100"
                    onClick={() => handleMegaMenu(id)}
                  >
                    <p>{title}</p>
                    <p>p</p>
                  </div>
                  {showMegaMenu && selectedNavLink === id && (
                    <>
                      <div>
                        {dropDownContent ? (
                          <div className="grid grid-row">
                            <div className="flex flex-wrap border-b-2 gap-x-2">
                              {dropDownContent.map(
                                ({ title, id, link, img }) => (
                                  <div
                                    key={id}
                                    className="flex p-2 gap-4 text-center items-center"
                                  >
                                    <img src={img} alt="" className="w-44" />
                                    <div>
                                      <p className="text-lg font-semibold">
                                        {title}
                                      </p>
                                      {link ? (
                                        <div className="flex  justify-center gap-2">
                                          {link.map((value, i) => (
                                            <p className="text-xs font-light">
                                              {value}
                                            </p>
                                          ))}
                                        </div>
                                      ) : null}
                                    </div>
                                  </div>
                                )
                              )}
                            </div>
                            <div className="px-10 py-5">
                              {dropDownContentRight.map(({ title, id }) => (
                                <p key={id} className="text-lg py-2">
                                  {title}
                                </p>
                              ))}
                            </div>
                          </div>
                        ) : (
                          <div className="flex gap-28">
                            {dropDownCenter ? (
                              <div className="flex gap-4">
                                {dropDownCenter.map(({ id, title, img }) => (
                                  <div key={id}>
                                    <img src={img} alt="" className="w-52" />
                                    <p className="text-center">{title}</p>
                                  </div>
                                ))}
                              </div>
                            ) : (
                              <>
                                <div className="px-2">
                                  {resource.map((item, i) => (
                                    <p
                                      key={i}
                                      className="py-4 text-lg font-medium"
                                    >
                                      {item}
                                    </p>
                                  ))}

                                  {locationServices.map((item, i) => (
                                    <p className="py-4 text-lg font-medium">
                                      {item}
                                    </p>
                                  ))}

                                  {about.map((item, i) => (
                                    <p className="py-4 text-lg font-medium">
                                      {item}
                                    </p>
                                  ))}
                                </div>
                              </>
                            )}
                          </div>
                        )}
                      </div>
                    </>
                  )}
                </>
              )
            )}

            <p className="text-lg rounded-md py-4 px-2 font-medium hover:bg-gray-200">
              Support
            </p>
            <p className="text-lg rounded-md py-4 px-2 font-medium hover:bg-gray-200">
              United States
            </p>
            <p className="text-lg rounded-md py-4 px-2 font-medium hover:bg-gray-200">
              Account
            </p>
          </div>
        )}
      </div>
    </header>
  );
};

export default Navbar;
