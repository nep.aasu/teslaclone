import {
  accessories,
  account,
  experienceTesla,
  megaMenuCharging,
  megaMenuCyberTruck,
  megaMenuHmc,
  megaMenuHomeCharging,
  megaMenuLifeStyle,
  megaMenuMegaPack,
  megaMenuModel3,
  megaMenuModels,
  megaMenuModelX,
  megaMenuModelY,
  megaMenuPowerwall,
  megaMenuShopApparel,
  megaMenuShopCharging,
  megaMenuShopVehicleAccessories,
  megaMenuSolarPanels,
  megaMenuSolarRoof,
  megaMenuSuperCharging,
  model,
  model3,
  modelS,
  modelX,
  modelY,
  powerWall,
  solarPanels,
  solarRoof,
  support,
  worldLanguage,
} from "../assets";

export const navLink = [
  {
    id: "vehicles",
    title: "Vehicles",
    dropDownContent: [
      {
        id: "modelS",
        title: "Model S",
        img: megaMenuModels,
        link: ["Learn", "Order"],
      },
      {
        id: "model3",
        title: "Model 3",
        img: megaMenuModel3,
        link: ["Learn", "Order"],
      },
      {
        id: "modelX",
        title: "Model X",
        img: megaMenuModelX,
        link: ["Learn", "Order"],
      },
      {
        id: "modelY",
        title: "Model Y",
        img: megaMenuModelY,
        link: ["Learn", "Order"],
      },
      {
        id: "CyberTruck",
        title: "Cyber Truck",
        img: megaMenuCyberTruck,
        link: ["Learn", "Order"],
      },
      {
        id: "helpMeChoose",
        title: "Help Me Choose",
        img: megaMenuHmc,
        link: ["Learn", "Order"],
      },
    ],
    dropDownContentRight: [
      {
        id: "inventory",
        title: "Inventory",
      },
      {
        id: "userCars",
        title: "User Cars",
      },
      {
        id: "demoDrive",
        title: "Demo Drive",
      },
      {
        id: "tradeIn",
        title: "Trade-in",
      },
      {
        id: "compare",
        title: "Compare",
      },
      {
        id: "helpMeCharge",
        title: "Help Me Charge",
      },
      {
        id: "fleet",
        title: "Fleet",
      },
      {
        id: "semi",
        title: "Semi",
      },
    ],
  },
  {
    id: "energy",
    title: "Energy",
    dropDownContent: [
      {
        id: "solarPanels",
        title: "Solar Panels",
        img: megaMenuSolarPanels,
        link: ["Learn", "Order"],
      },
      {
        id: "solarRoof",
        title: "Solar Roof",
        img: megaMenuSolarRoof,
        link: ["Learn", "Order"],
      },
      {
        id: "powerWall",
        title: "Powerwall",
        img: megaMenuPowerwall,
        link: ["Learn", "Order"],
      },
      {
        id: "megaPack",
        title: "Megapack",
        img: megaMenuMegaPack,
        link: ["Learn", "Order"],
      },
    ],
    dropDownContentRight: [
      {
        id: "whySolar",
        title: " Why Solar",
      },
      {
        id: "incentives",
        title: "Incentives",
      },
      {
        id: "support",
        title: "Support",
      },
      {
        id: "partnerWithTesla",
        title: "Partner With Tesla",
      },
      {
        id: "commercial",
        title: "Commercial",
      },
      {
        id: "utilities",
        title: "Utilities",
      },
    ],
  },
  {
    id: "charging",
    title: "Charging",
    dropDownContent: [
      {
        id: "charging",
        title: "Charging",
        img: megaMenuCharging,
        link: ["Learn"],
      },
      {
        id: "homeCharging",
        title: "Home Charging",
        img: megaMenuHomeCharging,
        link: ["Learn", "Shop"],
      },
      {
        id: "superCharging",
        title: "Super Charging",
        img: megaMenuSuperCharging,
        link: ["Learn", "Order"],
      },
    ],
    dropDownContentRight: [
      {
        id: "helpMeCharge",
        title: "Help Me Charge",
      },
      {
        id: "chargingCalculator",
        title: "Charging Calculator",
      },
      {
        id: "tripPlanner",
        title: "Trip Planner",
      },
      {
        id: "superChargerVoting",
        title: "SuperCharger Voting",
      },
      {
        id: "hostSuperCharger",
        title: "Host a Supercharger",
      },
      {
        id: "commercialCharging",
        title: "Commercial Charging",
      },
      {
        id: "hostWallCharging",
        title: "Host Wall Charging",
      },
    ],
  },
  {
    id: "discover",
    title: "Discover",
    resource: [
      "Demo Drive",
      "Insurance",
      "Video Guides",
      " Customer Stories",
      "Events",
    ],
    locationServices: [
      "Find Us",
      "Trip Planner",
      "Find a Collision Center",
      "Find a Certified Installer",
    ],
    about: ["About", "Careers", "Investor Relations"],
  },
  {
    id: "shop",
    title: "Shop",
    dropDownCenter: [
      {
        id: "charging",
        title: "Charging",
        img: megaMenuShopCharging,
      },
      {
        id: "vehicleAccessories",
        title: "Vehicle accessories",
        img: megaMenuShopVehicleAccessories,
      },
      {
        id: "apparel",
        title: "Apparel",
        img: megaMenuShopApparel,
      },
      {
        id: "lifeStyle",
        title: "Lifestyle",
        img: megaMenuLifeStyle,
      },
    ],
  },
];

export const rightNavLink = [
  {
    id: "support",
    title: "Support",
    img: support,
  },
  {
    id: "worldLanguage",
    title: "Language",
    img: worldLanguage,
  },
  {
    id: "account",
    title: "Account",
    img: account,
  },
];

export const hero = [
  {
    id: "model3",
    img: model3,
    title: "Model 3",
    desc: "Leasing starting at $349/mo",
    button: ["Custom Order", "Learn More"],
    desc1:
      "*Excludes taxes and fees with price subject to change. Available in select states. See Details",
  },
  {
    id: "modelY",
    video: modelY,
    title: "Model Y",
    desc: "Leasing starting at $349/mo",
    button: ["Custom Order", "Demo Drive"],
    desc1:
      "*Excludes taxes and fees with price subject to change. Available in select states. See Details",
  },
  {
    id: "modelX",
    img: modelX,
    title: "Model X",
    price: "From $65,590*",
    desc: "After Federal Tax Credit & Est. Gas Savings",
    button: ["Custom Order", "Demo Drive"],
    desc1:
      "Price before incentives and savings is $79,990, excluding taxes and fees. Subject to change. Learn about est. gas savings.",
  },
  {
    id: "modelS",
    img: modelS,
    price: "From $71,090*",
    title: "Model S",
    desc: "After Est. Gas Savings",
    button: ["Custom Order", "Demo Drive"],
    desc1:
      "Price before incentives and savings is $79,990, excluding taxes and fees. Subject to change.  Learn about est. gas savings.",
  },
  {
    id: "model",
    img: model,
    title: "",
    desc: "",
    button: ["Order Now", "Learn More"],
  },
  {
    id: "experienceTesla",
    video: experienceTesla,
    title: "Experience Tesla",
    desc: "Schedule a Demo Drive Today",
    button: ["Demo Drive"],
    desc1:
      "Price before incentives and savings is $79,990, excluding taxes and fees. Subject to change. Learn about est. gas savings.",
  },
  {
    id: "solarPanels",
    img: solarPanels,
    title: "Solar Panels",
    desc: "Schedule a Virtual Consultation",
    button: ["Custom Order", "View Inventory"],
  },
  {
    id: "solarRoof",
    img: solarRoof,
    title: "Solar Roof",
    desc: "Produce Clean Energy From Your Roof",
    button: ["Order Now", "Learn More"],
  },
  {
    id: "powerWall",
    img: powerWall,
    title: "Powerwall",
    desc: "",
    button: ["Order Now", "Learn More"],
  },
  {
    id: "accessories",
    img: accessories,
    title: "Accessories",
    desc: "",
    button: ["Shop More"],
  },
];
