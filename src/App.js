import Banner from "./component/Banner";
import Hero from "./component/Hero";
import Navbar from "./component/Navbar";

function App() {
  return (
    <div className="App">
      
      <Banner> </Banner>
      <Navbar></Navbar>
      <Hero></Hero>
    </div>
  );
}

export default App;
