// ScrollListener.js
import React from 'react';
import { animateScroll as scroll } from 'react-scroll';

const ScrollListener = ({ children }) => {
  const handleScroll = (e) => {
    if (e.deltaY !== 0) {
      // Scroll to the element with the ID 'section1'
      scroll.scrollTo('section1', {
        smooth: true,
        offset: -70,
        duration: 500,
      });
    }
  };

  return <div onWheel={handleScroll}>{children}</div>;
};

export default ScrollListener;
