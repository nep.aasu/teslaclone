import logo from "./logo.svg";
import right from "./right.svg";
import cross from "./cross.svg";
import account from "./account.svg";
import worldLanguage from "./worldLanguage.svg";
import accessories from "./accessories.avif";
import support from "./support.svg";
import model3 from "./model-3.avif";
import modelS from "./model-s.avif";
import modelX from "./model-x.avif";
import modelY from "./model-y.mp4";
import powerWall from "./powerwall.avif";
import solarPanels from "./solarPanels.avif";
import solarRoof from "./solarRoofs.avif";
import model from "./model.avif";
import experienceTesla from "./experience.mp4";

import megaMenuModels from "./Vehicles-mega-menu/Mega-Menu-Vehicles-Model-S.avif";
import megaMenuModel3 from "./Vehicles-mega-menu/Mega-Menu-Vehicles-Model-3-LHD.avif";
import megaMenuModelY from "./Vehicles-mega-menu/Mega-Menu-Vehicles-Model-Y.avif";
import megaMenuModelX from "./Vehicles-mega-menu/Mega-Menu-Vehicles-Model-X.avif";
import megaMenuCyberTruck from "./Vehicles-mega-menu/Mega-Menu-Vehicles-Cybertruck-1x.avif";
import megaMenuHmc from "./Vehicles-mega-menu/Mega-Menu-Vehicles-HMC-RedBlue-LHD.avif";

import megaMenuMegaPack from "./Energy-Mega-Menu/Mega-Menu-Energy-Megapack.avif";
import megaMenuPowerwall from "./Energy-Mega-Menu/Mega-Menu-Energy-Powerwall-US.avif";
import megaMenuSolarPanels from "./Energy-Mega-Menu/Mega-Menu-Energy-Solar-Panels.avif";
import megaMenuSolarRoof from "./Energy-Mega-Menu/Mega-Menu-Energy-Solar-Roof.avif";

import megaMenuCharging from "./Charging-mega-menu/Mega-Menu-Charging-Charging.avif";
import megaMenuHomeCharging from "./Charging-mega-menu/Mega-Menu-Charging-Home-Charging.avif";
import megaMenuSuperCharging from "./Charging-mega-menu/Mega-Menu-Charging-Supercharging-NA.avif";

import megaMenuShopApparel from "./Shop-mega-menu/Mega-Menu-Shop-Apparel.avif";
import megaMenuShopCharging from "./Shop-mega-menu/Mega-Menu-Shop-Charging.avif";
import megaMenuLifeStyle from "./Shop-mega-menu/Mega-Menu-Shop-Lifestyle.avif";
import megaMenuShopVehicleAccessories from "./Shop-mega-menu/Mega-Menu-Shop-Vehicle-Accessories.avif";

export {
  logo,
  right,
  worldLanguage,
  account,
  cross,
  support,
  model,
  accessories,
  model3,
  modelS,
  modelX,
  modelY,
  powerWall,
  experienceTesla,
  solarPanels,
  solarRoof,
  megaMenuModels,
  megaMenuModel3,
  megaMenuModelY,
  megaMenuModelX,
  megaMenuCyberTruck,
  megaMenuHmc,
  megaMenuMegaPack,
  megaMenuPowerwall,
  megaMenuSolarPanels,
  megaMenuSolarRoof,
  megaMenuCharging,
  megaMenuHomeCharging,
  megaMenuSuperCharging,
  megaMenuShopApparel,
  megaMenuShopCharging,
  megaMenuLifeStyle,
  megaMenuShopVehicleAccessories,
};
